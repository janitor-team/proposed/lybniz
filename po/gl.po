# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# AlexL <loginov.alex.valer@gmail.com>, 2019
# Xurxo Guerra Perez <xguerrap@gmail.com>, 2015-2016
msgid ""
msgstr ""
"Project-Id-Version: lybniz\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-19 09:38+0300\n"
"PO-Revision-Date: 2019-02-19 05:23+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>\n"
"Language-Team: Galician (http://www.transifex.com/Magic/lybniz/language/"
"gl/)\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../lybniz:345 ../lybniz:356 ../lybniz:367 ../lybniz:404
msgid "Function"
msgstr ""

#: ../lybniz:345 ../lybniz:356 ../lybniz:367
msgid "is invalid."
msgstr ""

#: ../lybniz:404
msgid "is invalid at"
msgstr ""

#: ../lybniz:439
msgid "_File"
msgstr "_Ficheiro"

#: ../lybniz:443
msgid "_Save"
msgstr "_Gardar"

#: ../lybniz:443
msgid "Save graph as bitmap"
msgstr "Gardar a gráfica coma mapa de bits"

#: ../lybniz:450
msgid "_Quit"
msgstr "_Saír"

#: ../lybniz:450
msgid "Quit Application"
msgstr "Saír da aplicación"

#: ../lybniz:458
msgid "_Graph"
msgstr "G_ráfica"

#: ../lybniz:462
msgid "P_lot"
msgstr "_Trazar"

#: ../lybniz:462
msgid "Plot Functions"
msgstr "Funcións de trazado"

#: ../lybniz:469
msgid "_Evaluate"
msgstr "_Avaliar"

#: ../lybniz:469
msgid "Evaluate Functions"
msgstr "Avaliar funcións"

#: ../lybniz:476
msgid "Zoom _In"
msgstr "Ampliar"

#: ../lybniz:476
msgid "Zoom In"
msgstr "Ampliar"

#: ../lybniz:483
msgid "Zoom _Out"
msgstr "Afastar"

#: ../lybniz:483
msgid "Zoom Out"
msgstr "Afastar"

#: ../lybniz:490
msgid "Zoom _Reset"
msgstr "Restabelecer ampliación"

#: ../lybniz:490
msgid "Zoom Reset"
msgstr "Restabelecer ampliación"

#: ../lybniz:497
msgid "_Connect Points"
msgstr "_Conectar puntos"

#: ../lybniz:504
msgid "Scale Style"
msgstr "Estilo da escala"

#: ../lybniz:508
msgid "Decimal"
msgstr "Decimal"

#: ../lybniz:508
msgid "Set style to decimal"
msgstr "Estabelecer o estilo a decimal"

#: ../lybniz:514
msgid "Radians π"
msgstr ""

#: ../lybniz:514
msgid "Set style to radians"
msgstr "Estabelecer o estilo a radiáns"

#: ../lybniz:520
msgid "Radians τ"
msgstr ""

#: ../lybniz:520
msgid "Set style to radians using Tau (τ)"
msgstr ""

#: ../lybniz:526
msgid "Custom"
msgstr "Personalizado"

#: ../lybniz:526
msgid "Set style to custom"
msgstr "Estabelecer o estilo personalizado"

#: ../lybniz:533
msgid "_Help"
msgstr "A_xuda"

#: ../lybniz:537
msgid "_Contents"
msgstr "_Contidos"

#: ../lybniz:537
msgid "Help Contents"
msgstr "Contidos da axuda"

#: ../lybniz:544
msgid "_About"
msgstr "_Sobre"

#: ../lybniz:544
msgid "About Box"
msgstr "Sobre"

#: ../lybniz:608
msgid "Evaluate"
msgstr "Avaliar"

#: ../lybniz:735
msgid "Save as..."
msgstr "Gardar como..."

#: ../lybniz:790
msgid "Function Graph Plotter"
msgstr "Función xeradora de gráficas"

#: ../lybniz:858
msgid "X min"
msgstr "X mín"

#: ../lybniz:863
msgid "Y min"
msgstr "Y mín"

#: ../lybniz:875
msgid "X max"
msgstr "X máx"

#: ../lybniz:881
msgid "Y max"
msgstr "Y máx"

#: ../lybniz:893
msgid "X scale"
msgstr "escala X"

#: ../lybniz:898
msgid "Y scale"
msgstr "escala Y"

#: ../lybniz.desktop:3
msgid "Lybniz"
msgstr "Lybniz"

#: ../lybniz.desktop:4
msgid "Lybniz Graph Plotter"
msgstr "Xerador de gráficas Lybniz"

#: ../lybniz.desktop:42
msgid "Plot graphs and functions"
msgstr "Trazar gráficas e funcións"

#: ../lybniz.desktop:90
msgid "lybniz"
msgstr "lybniz"
